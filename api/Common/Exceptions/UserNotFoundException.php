<?php
namespace Api\Common\Exceptions;

/**
 * File UserNotFoundException.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Exceptions
 * @subpackage Exception.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class UserNotFoundException
 *
 * Triggered for not found
 *
 * @package   Api\Common\Exceptions;
 * @subpackage Exception
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UserNotFoundException extends Exception
{

}