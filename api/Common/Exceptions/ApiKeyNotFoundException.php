<?php namespace Api\Common\Exceptions;

/**
 * File ApiKeyNotFoundException.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Exceptions
 * @subpackage Exception.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class ApiKeyNotFoundException
 *
 * Triggers when Api Key not found
 *
 * @package    Api\Common\Exceptions;
 * @subpackage Exception
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ApiKeyNotFoundException extends Exception
{

}