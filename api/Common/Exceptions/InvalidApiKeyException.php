<?php namespace Api\Common\Exceptions;

/**
 * File InvalidApiKeyException.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Exceptions
 * @subpackage Exception.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class InvalidApiKeyException
 *
 * Triggered fro invalid API Key
 *
 * @package    Api\Common\Exceptions;
 * @subpackage Exception
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class InvalidApiKeyException extends Exception
{

}