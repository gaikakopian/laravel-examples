<?php namespace Api\Common\Errors;

/**
 * File OtherApplicationError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Errors
 * @subpackage OtherApplicationError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class OtherApplicationError
 *
 * Generate additional message when OtherApplicationException is thrown
 *
 * @package    Api\Common\Errors;
 * @subpackage OtherApplicationError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class OtherApplicationError extends Error
{
    /**
     * @const int
     */
    const CODE = 1000;

    /**
     * @const string
     */
    const MESSAGE = 'Other Application Error';
}