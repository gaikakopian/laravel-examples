<?php namespace Api\Common\Errors;

/**
 * File ApiKeyNotFoundError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Errors
 * @subpackage OtherApplicationError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class ApiKeyNotFoundError
 *
 * Generate additional message when ApiKeyNotFoundException is thrown
 *
 * @package    Api\Common\Errors;
 * @subpackage OtherApplicationError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ApiKeyNotFoundError extends Error
{
    /**
     * @const int
     */
    const CODE = 3000;

    /**
     * @const string
     */
    const MESSAGE = 'API Key Error: ';
}