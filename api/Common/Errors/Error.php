<?php namespace Api\Common\Errors;

/**
 * File Error.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Errors
 * @subpackage Error.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class Error
 *
 * @package    Api\Common\Errors;
 * @subpackage Error
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class Error implements \JsonSerializable
{
    /**
     * @const int
     */
    const CODE = 0;
    /**
     * @const string
     */
    const MESSAGE = '';

    /**
     * @var string[]
     */
    protected $additionalData;

    private   $errorCodeDisplay;

    /**
     * Error constructor.
     *
     * @param       $errorCodePrefix
     * @param array $additionalData
     */
    public function __construct($errorCodePrefix, $additionalData = [])
    {
        $this->errorCodeDisplay = $errorCodePrefix . static::CODE;
        $this->additionalData   =  $additionalData;
    }

    // function called when encoded with json_encode
    public function jsonSerialize()
    {
        $return          = new \stdClass();
        $return->code    = $this->errorCodeDisplay;
        $return->message = static::MESSAGE;
        if (!empty($this->additionalData))
        {
            $return->additionalData = $this->additionalData;
        }

        return $return;
    }
}