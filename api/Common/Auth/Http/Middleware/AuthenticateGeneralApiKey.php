<?php
namespace Api\Common\Auth\Http\Middleware;

/**
 * File AuthenticateGeneralApiKey.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\Common\Auth\Http\Middleware
 * @subpackage AuthenticateGeneralApiKey.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\ApiKeyNotFoundException;
use Api\Common\Exceptions\Exception;
use Api\Common\Exceptions\InvalidApiKeyException;
use Api\Common\Response;
use Closure;
use Illuminate\Http\Request;


/**
 * Class AuthenticateGeneralApiKey
 *
 * Verify and Authenticate Api Key passed on header or querystring
 *
 * @package    Api\Common\Auth\Http\Middleware;
 * @subpackage AuthenticateGeneralApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class AuthenticateGeneralApiKey
{

    /**
     * Handle an incoming request.
     *
     * @param  Request     $request
     * @param  Closure      $next
     * @param  string|null $guard
     *
     * @throws ApiKeyNotFoundException
     * @throws InvalidApiKeyException
     *
     * @return Closure|Response
     */
    public function handle(Request $request, Closure $next)
    {
        try
        {
            $apiKeyValueFromHeader = $request->header('Authorization');

            if (empty($apiKeyValueFromHeader))
            {
                throw new ApiKeyNotFoundException("API Key Not Found"); // API Key not provided
            }

            $bearer           = explode(' ', $apiKeyValueFromHeader);
            $apiKeyValue      = isset($bearer[1]) ? $bearer[1] : $bearer[0];

            if (!array_key_exists('general_api_key' , config('instasuite')))
            {
                throw new InvalidApiKeyException("Unauthorized Access!");
            }

            if ($apiKeyValue != config('instasuite')['general_api_key'])
            {
                throw new InvalidApiKeyException("Unauthorized Access!"); // Wrong api key provided
            }

            return $next($request);
        }
        catch (Exception $e)
        {
            $resp = new Response(null);

            return $resp->setErrorFromException(100, $e);
        }
    }

}