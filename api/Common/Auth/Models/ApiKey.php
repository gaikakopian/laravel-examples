<?php namespace Api\Common\Auth\Models;

/**
 * File ApiKey.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Sujit\Api\Auth\Models
 * @subpackage ApiKey.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Request;


/**
 * Class ApiKey
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ApiKey extends Eloquent
{

    protected $connection = 'mongodb';
    protected $collection = 'api_keys';

    protected $fillable = [
        'key',
        'apikeyable_id',
        'apikeyable_type',
        'last_ip_address',
        'last_used_at',
        'note'
    ];

    /**
     * Set Morph Relation to desired model
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function apikeyable()
    {
        return $this->morphTo();
    }

    /**
     * Create Api Key for given ID of model
     *
     * @param Model|ApiKeyable $apikeyable
     *
     * @return ApiKey
     */
    public static function make(Model $apikeyable , $note)
    {
        $apiKey = new ApiKey([
            'key'             => self::generateKey(),
            'apikeyable_id'   => $apikeyable->id,
            'apikeyable_type' => get_class($apikeyable),
            'last_ip_address' => Request::ip(),
            'last_used_at'    => Carbon::now(),
            'note' => $note
        ]);
        $apiKey->save();

        return $apiKey;
    }

    /**
     * Method to generate a unique API key
     *
     * @return string
     */
    public static function generateKey()
    {
        do
        {
            $token = bin2hex(openssl_random_pseudo_bytes(32));
        } // Already in the DB? Fail. Try again
        while (self::keyExists($token));

        return $token;
    }

    /**
     * Checks whether a key exists in the database or not
     *
     * @param $key
     *
     * @return bool
     */
    private static function keyExists($key)
    {

        $apiKeyCount = self::where('key', $key)->limit(1)->count();

        if ($apiKeyCount > 0)
        {
            return true;
        }

        return false;
    }
}