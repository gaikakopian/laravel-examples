<?php namespace Api\V1\General\Errors;

/**
 * File UserAlreadyIsAdminError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Errors
 * @subpackage UserAlreadyIsAdminError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Errors\Error;

/**
 * Class UserAlreadyIsAdminError
 *
 * Generate additional message when CannotCreateUserException is thrown
 *
 * @package    Api\V1\SmartMailer\Errors;
 * @subpackage UserAlreadyIsAdminError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UserAlreadyIsAdminError extends Error
{
    /**
     * @const int
     */
    const CODE = 1003;

    /**
     * @const string
     */
    const MESSAGE = 'Cannot Assign Admin Role: ';
}