<?php namespace Api\V1\General\Errors;

/**
 * File InvalidUserRegistration.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Errors
 * @subpackage InvalidUserRegistration.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class InvalidUserRegistration
 *
 * Generate additional message when InvalidUserRegistrationException is thrown
 *
 * @package    Api\V1\SmartMailer\Errors;
 * @subpackage InvalidUserRegistration
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class InvalidUserRegistrationError extends ValidationError
{
    /**
     * @const int
     */
    const CODE = 1001;

    /**
     * @const string
     */
    const MESSAGE = 'Invalid User Registration';
}