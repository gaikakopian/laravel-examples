<?php namespace Api\V1\General\Errors;

/**
 * File ValidationError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Errors
 * @subpackage ValidationError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Errors\Error;

/**
 * Class ValidationError
 *
 *  Generate additional message when ValidationException is thrown
 *
 * @package    Api\V1\SmartMailer\Errors;
 * @subpackage ValidationError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ValidationError extends Error
{
    /**
     * @const int
     */
    const CODE = '1210';

    /**
     * @const string
     */
    const MESSAGE = 'Validation Error';

}