<?php namespace Api\V1\General\Errors;

/**
 * File CannotCreateUserError.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Errors
 * @subpackage CannotCreateUserError.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class CannotCreateUserError
 *
 * Generate additional message when CannotCreateUserException is thrown
 *
 * @package    Api\V1\SmartMailer\Errors;
 * @subpackage CannotCreateUserError
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CannotGetLinkTypeError extends ValidationError
{
    /**
     * @const int
     */
    const CODE = 1002;

    /**
     * @const string
     */
    const MESSAGE = 'Cannot get link type';
}