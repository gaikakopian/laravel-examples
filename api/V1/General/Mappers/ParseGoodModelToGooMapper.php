<?php
namespace Api\V1\General\Mappers;

/**
 * File UserModelToUserMapper.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Mappers
 * @subpackage UserModelToUserMapper.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Entities\GoodEntity;
use Api\V1\General\Entities\UserEntity;

/**
 * Class UserModelToUserMapper
 *
 * Parse the Model object to entity
 *
 * @package    Api\V1\General\Mappers;
 * @subpackage UserModelToUserMapper
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ParseGoodModelToGooMapper
{
    /**
     * Map the model to entity
     *
     * @param      $user
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return UserEntity
     */
    public static function map($good)
    {
        return new GoodEntity([
            'goodId'    => $good->id,
            'category_id'  => $good->category_id,
            'category'  => $good->category,
            'tags'  => $good->tags,
            'user_id' => $good->user_id,
            'title'  => $good->title,
            'price'  => $good->price,
            'description' => $good->description,
            'short_description' => !empty(substr($good->description,0,2)) ? substr($good->description,0,2) : '',
            'photo' => $good->photo,
            'photo_description' => $good->photo_description,
            'createdAt' => $good->created_at,
        ]);
    }

}