<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\CannotGetLinkException;
use Api\V1\General\Exceptions\InvalidDataException;
use App\Models\Link;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class LinksService extends Service
{
    public function create($request){
        try{
            $data = $request->all();
            $link = new Link();

            $data['show_social_icons'] = !empty($data['show_social_icons']) == 'true' ? true : false;
            $data['link_shops'] = \GuzzleHttp\json_decode($request->link_shops);


            if(!empty($request->photo)){
                $url = Storage::put('/links', $request->photo);
                $data['preview_image'] = '/uploads/' . $url;
            }

            $data['last_scrawled'] = Carbon::now()->toDateTimeString();

            $created = $link->create($data);

            return $created;

        } catch (\Exception $e) {
            throw new InvalidDataException($e->getMessage());
        }
    }

    public function getLink($request){
        try{

            $link = Link::find($request->id);
            return $link;

        } catch (\Exception $e) {
            throw new InvalidDataException($e->getMessage());
        }
    }

    public function getList($request){
        try{

            $links = Link::where('account_id',$request->account_id)->get();
            return $links;

        } catch (\Exception $e) {
            throw new InvalidDataException($e->getMessage());
        }
    }

    public function edit($request){
        try{
            $link = Link::find($request->id);

            $data = $request->all();

            $data['link_shops'] = \GuzzleHttp\json_decode($request->link_shops);
            $data['show_social_icons'] = !empty($data['show_social_icons'])  == 'true' ? true : false;

            //Checking if image is uploaded.
            if(!is_null($request->photo) && !empty($request->photo)){
                $data['preview_image'] = Storage::put('/links', $request->photo);
                Storage::delete($link->preview_image);
            }

            // Checking whether user wants to edit his info.
            if($request->delete_avatar && !is_null($link->avatar)){
                Storage::delete($link->preview_image);
                $data['preview_image'] = NULL;
            }

            $link->update($data);

            return $link;

        } catch (\Exception $e) {
            throw new InvalidDataException($e->getMessage());
        }
    }

    public function delete($request){
        try{
            $link = Link::find($request->id);
            $link->delete();

            return true;

        } catch (\Exception $e) {
            throw new InvalidDataException($e->getMessage());
        }
    }

    public function getLinkSlug($request){
        try{

            $link = Link::where('slug',$request->slug)->first();
            if(!$link){
                throw new CannotGetLinkException($e->getMessage());
            }
            return $link;

        } catch (\Exception $e) {
            throw new InvalidDataException($e->getMessage());
        }
    }

    public function search($request){
        try{
            $q = $request->q;

            $links = Link::where('account_id', $request->account_id)
                ->orWhere('title', 'like', '%' . $q . '%')
                ->orWhere('artist', 'like', '%' . $q. '%')
                ->orWhere('description', 'like', '%' . $q . '%')
                ->orWhere('slug', 'like', '%' . $q . '%')->get();

            return $links;

        } catch (\Exception $e) {
            throw new InvalidDataException($e->getMessage());
        }
    }
}