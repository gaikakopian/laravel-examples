<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\CannotCreateUserException;
use App\Models\Click;
use App\Models\Link;
use Illuminate\Support\Facades\Request;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ClicksService extends Service
{
    public function create($request){
        try{
            $data = [];

            $link = Link::select('_id')->where('slug',$request->slug)->first();

            $data['link_id'] = $link->_id;
            $data['ip_address'] = Request::ip();

            $click = Click::create($data);

            return $click;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }

    public function preview($request){
        try{
            $click = Click::find($request->id);
            $click->is_previewed = true;
            $click->save();

            return true;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }

    public function service($request){
        try{
            $click = Click::find($request->click_id);
            $click->is_clicked_to_service = true;
            $click->clicked_service_id = $request->service_id;
            $click->save();

            return true;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }

    public function count($request){
        try{
            $data = [];

            $clicks = Click::select('_id','is_clicked_to_service','is_previewed')->where('link_id',$request->link_id)->get();

            $data['total_count'] = count($clicks);
            $data['is_previewed'] = 0;
            $data['is_clicked_to_service'] = 0;

            foreach ($clicks as $click){
                if($click->is_previewed){
                    $data['is_previewed']++;
                    continue;
                }
                if($click->is_clicked_to_service){
                    $data['is_clicked_to_service']++;
                    continue;
                }
            }

            return $data;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }
}