<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Auth\Models\ApiKey;
use Api\V1\General\Exceptions\CannotCreateUserException;
use App\Models\Country;
use App\User;
use Api\Common\Exceptions\Exception;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CountriesService extends Service
{
    public function getList($request){
        try{
            $countries = Country::select('id','title','code','icon_image')->get();

            return $countries;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }
}