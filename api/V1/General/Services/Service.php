<?php
namespace Api\V1\General\Services;

/**
 * File Service.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @subpackage Service.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

/**
 * Class Service
 *
 * @package    Api\V1\General\Services;
 * @subpackage Service
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class Service
{

}