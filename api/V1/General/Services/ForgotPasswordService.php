<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\CannotResetPasswordException;
use Api\V1\General\Exceptions\NoUserFoundWithUsernameException;
use Api\V1\General\Models\UCodeMessage;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ForgotPasswordService extends Service
{
    use SendsPasswordResetEmails,ValidatesRequests;

    public function sendRequest($request){
        try{
            $this->sendResetLinkEmail($request);
            return true;
        } catch (\Exception $e) {
            throw new CannotResetPasswordException($e->getMessage());
        }
    }
}