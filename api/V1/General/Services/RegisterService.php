<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Auth\Models\ApiKey;
use Api\V1\General\Exceptions\CannotCreateUserException;
use Api\V1\General\Models\UUser;
use App\User;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Api\Common\Exceptions\Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class RegisterService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    public function register($request){
        try{
            $data = [];

            $user = $this->create($request->all());

            $apikey = ApiKey::make($user,'user registration with email');

            $data['user'] = $user;
            $data['token'] = $apikey->key;

            return $data;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }

    protected function create(array $data)
    {
        return UUser::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}