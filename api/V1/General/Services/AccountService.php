<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Auth\Models\ApiKey;
use Api\V1\General\Exceptions\CannotCreateUserException;
use App\Models\AccountPermission;
use App\User;
use Api\Common\Exceptions\Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Models\Account;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class AccountService extends Service
{
    public function create($request){
        try{
            $data = $request->all();

            $data['admin_id'] = $request->apiKey->apikeyable_id;
            //Checking if image is uploaded.
            if(!is_null($request->photo)){
                $data['avatar'] = Storage::put('/images', $request->photo);
            }
            $account = Account::create($data);

            return $account;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }

    public function edit($request){
        try{
            $account = Account::find($request->id);

            $data = $request->all();

            $data['admin_id'] = $request->apiKey->apikeyable_id;

            //Checking if image is uploaded.
            if(!is_null($request->photo)){
                $data['avatar'] = Storage::put('/images', $request->photo);
                Storage::delete($account->avatar);
            }

            // Checking whether user wants to edit his info.
            if($request->delete_avatar && !is_null($user->avatar)){
                Storage::delete($account->avatar);
                $data['avatar'] = NULL;
            }

            $account->update($data);

            return $account;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }

    public function delete($request){
        try{

            $account = Account::find($request->id);
            $account->delete();

            return true;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }

    public function givePermission($request){
        try{
            $permission_exist = AccountPermission::where('account_id',$request->account_id)->where('user_id',$request->user_id)->first();
            if($permission_exist){
                $permission_exist->delete();
            }

            $permission = AccountPermission::create($request->all());

            return $permission;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }

    public function deletePermission($request){
        try{
            $permission = AccountPermission::find($request->id);
            $permission->delete();

            return true;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }

    public function getList($request){
        try{
            $accounts = Account::where('admin_id',$request->apiKey->apikeyable_id)->get();

            return $accounts;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }

    public function getAccount($request){
        try{
            $account = Account::where('_id', $request->id)->where('admin_id',$request->apiKey->apikeyable_id)->first();

            return $account;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }
}