<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\CannotResolveUrlException;
use Api\V1\General\Integrations\Beatport;
use Api\V1\General\Integrations\Deezer;
use Api\V1\General\Integrations\Itunes;
use Api\V1\General\Integrations\SoundCloud;
use Api\V1\General\Integrations\Spotify;
use Api\V1\General\Integrations\Youtube;
use App\Models\Link;
use Revolution\Amazon\ProductAdvertising\Facades\AmazonProduct;
use MoussaClarke\BeatportApi;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ResolverService extends Service
{
    public function resolve($request){
        try{
            $data = [];

            switch ($request->type){
                case 'spotify':

                    //Getting track id from link
                    $parts = explode("/", $request->q);
                    $track_id = end($parts);

                    $spotify = new Spotify();
                    $trackData = $spotify->getTrack($track_id);

                    $data['artist'] = $trackData->artists[0]->name;
                    $data['track_name'] = $trackData->name;
                    $data['preview_url'] = $trackData->preview_url;
                    $data['image'] = $trackData->album->images ? $trackData->album->images[0] : null;
                    $data['source_id'] = $trackData->id;

                    break;
                case 'amazon':
                    $response = AmazonProduct::item($request->q);
                    return $response;

                    break;
                case 'beatport':
                    $trackData = $this->getBeatPortTrack($request->q);

                    if(!empty($trackData['results'][0])){
                        $data['artist'] = $trackData['results'][0]['artists'][0]['name'];
                        $data['track_name'] = $trackData['results'][0]['title'];
                        $data['image'] = $trackData['results'][0]['images']['large']['url'];
                        $data['source_id'] = $trackData['results'][0]['id'];
                    }
                    // auth parameters
                    break;
                case 'deezer':

                    $trackData = $this->getDeezerTrack($request->q);

                    $data['artist'] = $trackData->artist->name;
                    $data['track_name'] = $trackData->title;
                    $data['preview_url'] = $trackData->preview;
                    $data['image'] = $trackData->album->cover_big;
                    $data['source_id'] = $trackData->id;

                    break;
                case 'youtube':

                    $trackData = $this->getYouTubeTrack($request->q);

                    if($trackData->items[0]){
                        $titleData = $this->getYouTubeTrackTitleAuthor($trackData->items[0]->snippet->title);

                        $data['artist'] = $titleData['author'];
                        $data['track_name'] = $titleData['title'];
                        $data['image'] = !empty($trackData->items[0]->snippet->thumbnails->standard) ? $trackData->items[0]->snippet->thumbnails->standard->url : $trackData->items[0]->snippet->thumbnails->default->url;
                        $data['source_id'] = $trackData->items[0]->id;
                    }

                    break;
                case 'soundcloud':

                    $trackData = $this->getSoundCloudTrack($request->q);

                    if($trackData){
                        $titleData = $this->getYouTubeTrackTitleAuthor($trackData->title);

                        $data['artist'] = $titleData['author'];
                        $data['track_name'] = $titleData['title'];
                        $data['image'] = !empty($trackData->artwork_url) ? $trackData->artwork_url : null;
                        $data['source_id'] = $trackData->id;
                    }

                    break;
                case 'itunes':

                    $trackData = $this->getItunesTrack($request->q);

                    if($trackData->results[0]){

                        $data['artist'] = $trackData->results[0]->artistName;
                        $data['track_name'] = $trackData->results[0]->trackName;
                        $data['image'] = !empty($trackData->results[0]->artworkUrl100) ? $trackData->results[0]->artworkUrl100 : null;
                        $data['source_id'] = $trackData->results[0]->trackId;
                        $data['preview_url'] = $trackData->results[0]->previewUrl;
                    }

                    break;
                case 'amazon_music':

                    $trackData = $this->checkAmazonMusicTrack($request->q);

                    if($trackData['Items']['Item']){

                        $data['artist'] = $trackData['Items']['Item']['ItemAttributes']['Artist'] ? $trackData['Items']['Item']['ItemAttributes']['Artist'] : $trackData['Items']['Item']['ItemAttributes']['Creator'];
                        $data['track_name'] = $trackData['Items']['Item']['ItemAttributes']['Title'];
                        $data['image'] = !empty($trackData['Items']['Item']['LargeImage']['URL']) ? $trackData['Items']['Item']['LargeImage']['URL'] : null;
                        $data['source_id'] = $trackData['Items']['Item']['ASIN'];
                    }

                    break;
            }

            return $data;
        } catch (\Exception $e) {
            throw new CannotResolveUrlException($e->getMessage());
        }

    }

    public function getType($request){
        try{
            $response = [];
            $response['is_valid'] = false;
            $response['type'] = null;
            $response['link_type'] = null;

            //Checking whether spotify link is entered
            if(strpos($request->link,'spotify.com')){

                //checking if Spotify track
                if(strpos($request->link,'spotify.com/track')){

                    $track = $this->checkSpotifyTrack($request->link);

                    if($track){
                        $response['is_valid'] = true;
                        $response['type'] = 'spotify';
                        $response['link_type'] = 'track';
                        $response['author'] = $track['artist'];
                        $response['title'] = $track['track_name'];
                        $response['img'] = $track['image']->url;
                        $response['track_data'] = $track;
                        $response['link'] = $request->link;
                        $response['slug'] = $track['slug'];
                        $response['preview_url'] = $track['preview_url'];
                        $response['source_id'] = $track['source_id'];
                    }
                }
            }
            //Checking whether amazon link is entered
            if(strlen ( $request->link ) == 10){

                $result = AmazonProduct::item($request->link);

                if($result['Items']['Item']){
                    $response['is_valid'] = true;
                    $response['type'] = 'amazon';
                    $response['link_type'] = 'product';
                    $response['author'] = !empty($result['Items']['Item']['ItemAttributes']['Author']) ? $result['Items']['Item']['ItemAttributes']['Author'] : '';
                    $response['title'] = $result['Items']['Item']['ItemAttributes']['Title'];
                    $response['img'] = $result['Items']['Item']['LargeImage']['URL'];
                    $response['link'] = $result['Items']['Item']['DetailPageURL'];
                    $response['manufacturer'] = $result['Items']['Item']['ItemAttributes']['Manufacturer'];
                    $response['slug'] = $this->generateSlug($response['title']);
                }
            }
            // Checking whether amazon music link
            if(strpos($request->link,'amazon')){

                $track = $this->checkAmazonMusicTrack($request->link);

                //checking if Amazon Music track
                if($track['Items']['Item']){

                    $response['is_valid'] = true;
                    $response['type'] = 'amazon_music';
                    $response['link_type'] = 'track';
                    $response['author'] = $track['Items']['Item']['ItemAttributes']['Artist'] ? $track['Items']['Item']['ItemAttributes']['Artist'] : $track['Items']['Item']['ItemAttributes']['Creator'];
                    $response['link'] = $track['Items']['Item']['DetailPageURL'];
                    $response['title'] = $track['Items']['Item']['ItemAttributes']['Title'];
                    $response['img'] = $track['Items']['Item']['LargeImage']['URL'];
                    $response['slug'] = $this->generateSlug($track['Items']['Item']['ItemAttributes']['Title']);
                    $response['source_id'] = $track['Items']['Item']['ASIN'];
                }
            }
            //Checking whether Deezer link is entered
            if(strpos($request->link,'deezer.com')){

                //checking if Deezer track
                if(strpos($request->link,'/track/')){
                    $deezer = $this->getDeezerTrack($request->link);

                    $response['is_valid'] = true;
                    $response['type'] = 'deezer';
                    $response['link_type'] = 'track';
                    $response['author'] = $deezer->artist->name;
                    $response['link'] = $deezer->link;
                    $response['title'] = $deezer->title;
                    $response['img'] = $deezer->album->cover_big;
                    $response['preview_url'] = $deezer->preview;
                    $response['slug'] = $this->generateSlug($deezer->title);
                    $response['source_id'] = $deezer->id;
                }
            }

            //Checking whether BeatPort link is entered
            if(strpos($request->link,'beatport.com')){

                //checking if BeatPort track
                if(strpos($request->link,'/track/')){
                    $beatport = $this->getBeatPortTrack($request->link);

                    if($beatport['results'][0]){
                        $response['is_valid'] = true;
                        $response['type'] = 'beatport';
                        $response['link_type'] = 'track';
                        $response['author'] = $beatport['results'][0]['artists'][0]['name'];
                        $response['link'] = $request->link;
                        $response['title'] = $beatport['results'][0]['title'];
                        $response['img'] = $beatport['results'][0]['images']['large']['url'];
                        $response['slug'] = $this->generateSlug($beatport['results'][0]['title']);
                        $response['source_id'] = $beatport['results'][0]['id'];
                    }
                }
            }

            //Checking whether Youtube link is entered
            if(strpos($request->link,'youtube.com')){

                //checking if Youtube video
                if(strpos($request->link,'/watch')){
                    $youtube = $this->getYoutubeTrack($request->link);

                    if($youtube->items[0]){

                        $titleData = $this->getYouTubeTrackTitleAuthor($youtube->items[0]->snippet->title);

                        $response['is_valid'] = true;
                        $response['type'] = 'youtube';
                        $response['link_type'] = 'track';
                        $response['author'] = $titleData['author'];
                        $response['link'] = $request->link;
                        $response['title'] = $titleData['title'];
                        $response['img'] = !empty($youtube->items[0]->snippet->thumbnails->standard) ? $youtube->items[0]->snippet->thumbnails->standard->url : $youtube->items[0]->snippet->thumbnails->default->url;
                        $response['slug'] = $this->generateSlug($titleData['title']);
                        $response['source_id'] = $youtube->items[0]->id;
                    }
                }
            }

            //Checking whether Soundcloud link is entered
            if(strpos($request->link,'soundcloud.com')){

                    $soundcloud = $this->getSoundCloudTrack($request->link);
                    if($soundcloud){
                        $titleData = $this->getYouTubeTrackTitleAuthor($soundcloud->title);

                        $response['is_valid'] = true;
                        $response['type'] = 'soundcloud';
                        $response['link_type'] = 'track';
                        $response['author'] = $titleData['author'];
                        $response['link'] = $soundcloud->permalink_url;
                        $response['title'] = $titleData['title'];
                        $response['img'] = !empty($soundcloud->artwork_url) ? $soundcloud->artwork_url : null;
                        $response['slug'] = $this->generateSlug($titleData['title']);
                        $response['source_id'] = $soundcloud->id;
                    }
            }

            //Checking whether Soundcloud link is entered
            if(strpos($request->link,'itunes.apple.com')){

                $itunes = $this->getItunesTrack($request->link);

                if($itunes->results[0]){

                    $response['is_valid'] = true;
                    $response['type'] = 'itunes';
                    $response['link_type'] = 'track';
                    $response['author'] = $itunes->results[0]->artistName;
                    $response['link'] = $itunes->results[0]->trackViewUrl;
                    $response['title'] = $itunes->results[0]->trackName;
                    $response['img'] = !empty($itunes->results[0]->artworkUrl100) ? $itunes->results[0]->artworkUrl100 : null;
                    $response['slug'] = $this->generateSlug($itunes->results[0]->trackName);
                    $response['source_id'] = $itunes->results[0]->trackId;
                    $response['preview_url'] = $itunes->results[0]->previewUrl;
                }
            }

            return $response;
        } catch (\Exception $e) {
            throw new CannotResolveUrlException($e->getMessage());
        }

    }

    public function scan($request){

        $data = [];

        if($request->type != 'spotify'){
            $result = $this->searchSpotifyTrack($request->title,$request->artist);
            if($result){
                array_push($data,$result);
            }
        }

        if($request->type != 'deezer'){
            $result = $this->searchDeezerTrack($request->title,$request->artist);
            if($result){
                array_push($data,$result);
            }
        }

        if($request->type != 'beatport'){
            $result = $this->searchBeatPortTrack($request->title,$request->artist);

            if($result){
                array_push($data,$result);
            }
        }

        if($request->type != 'youtube'){
            $result = $this->searchYoutubeTrack($request->title,$request->artist);

            if($result){
                array_push($data,$result);
            }
        }

        if($request->type != 'soundcloud'){
            $result = $this->searchSoundCloudTrack($request->title,$request->artist);

            if($result){
                array_push($data,$result);
            }
        }

        if($request->type != 'itunes'){
            $result = $this->searchItunesTrack($request->title,$request->artist);

            if($result){
                array_push($data,$result);
            }
        }

        if($request->type != 'amazon_music'){
            $result = $this->searchAmazonTrack($request->title,$request->artist);

            if($result){
                array_push($data,$result);
            }
        }

        return $data;

    }

    public function checkSlug($request){

        $result = false;
        $link = Link::select('slug')->where('slug',$request->slug)->first();
        if(!$link){
            $result = true;
        }

        return $result;
    }

    private function searchSpotifyTrack($title,$artist){

        try{
            $spotify = new Spotify();
            $trackData = $spotify->search($title,$artist,'track');

            if(!empty($trackData->tracks->items[0])){
                $data = [];
                $data['type'] = 'spotify';
                $data['artist'] = $trackData->tracks->items[0]->artists[0]->name;
                $data['image'] = $trackData->tracks->items[0]->album->images ? $trackData->tracks->items[0]->album->images[0] : null;
                $data['link'] = $trackData->tracks->items[0]->external_urls->spotify;

                $data['track_name'] = $trackData->tracks->items[0]->name;
                $data['preview_url'] = $trackData->tracks->items[0]->preview_url;

                $data['source_id'] = $trackData->tracks->items[0]->id;

                $data['markets'] = $trackData->tracks->items[0]->available_markets;

                return $data;
            }
            return false;

        } catch (\Exception $e) {
            return false;
        }
    }

    private function searchDeezerTrack($title,$artist){

        try{
            $deezer = new Deezer();
            $trackData = $deezer->search($title,$artist,'track');

            if(!empty($trackData->data[0])){
                $data = [];
                $data['type'] = 'deezer';
                $data['artist'] = $trackData->data[0]->artist->name;
                $data['image'] = $trackData->data[0]->album->cover_big;
                $data['link'] = $trackData->data[0]->link;

                $data['track_name'] = $trackData->data[0]->title;
                $data['preview_url'] = $trackData->data[0]->preview;

                $data['source_id'] = $trackData->data[0]->id;

                $data['markets'] = null;

                return $data;
            }
            return false;

        } catch (\Exception $e) {
            return false;
        }
    }

    private function searchBeatPortTrack($title,$artist){

        try{
            $beatport = new Beatport();
            $trackData = $beatport->search($title,$artist);


            if(!empty($trackData['results'][0])){
                $data = [];
                $data['type'] = 'beatport';

                //$data['artist'] = $trackData['results'][0]['artists'][0];
                $data['image'] = $trackData['results'][0]['images']['large']['url'];

                $data['link'] = 'https://beatport.com/track/' . $trackData['results'][0]['slug'] . '/' . $trackData['results'][0]['id'];
                $data['track_name'] = $trackData['results'][0]['name'];

                $data['source_id'] = $trackData['results'][0]['id'];

                $data['markets'] = null;

                return $data;
            }
            return false;

        } catch (\Exception $e) {
            return false;
        }
    }

    private function searchYoutubeTrack($title,$artist){

        try{
            $youtube = new Youtube();
            $trackData = $youtube->search($title,$artist);

            if(!empty($trackData->items[0])){

                $titleData = $this->getYouTubeTrackTitleAuthor($trackData->items[0]->snippet->title);

                $data = [];
                $data['type'] = 'youtube';

                $data['artist'] = $titleData['author'];
                $data['image'] = !empty($trackData->items[0]->snippet->thumbnails->standard) ? $trackData->items[0]->snippet->thumbnails->standard->url : $trackData->items[0]->snippet->thumbnails->default->url;

                $data['link'] = 'https://www.youtube.com/watch?v=' . $trackData->items[0]->id->videoId;
                $data['track_name'] = $titleData['title'];

                $data['source_id'] = $trackData->items[0]->id->videoId;

                $data['markets'] = null;

                return $data;
            }
            return false;

        } catch (\Exception $e) {
            return false;
        }
    }

    private function searchSoundCloudTrack($title,$artist){

        try{
            $soundcloud = new SoundCloud();
            $trackData = $soundcloud->search($title,$artist);

            if($trackData[0]){

                $titleData = $this->getYouTubeTrackTitleAuthor($trackData[0]->title);

                $data = [];
                $data['type'] = 'soundcloud';

                $data['artist'] = $titleData['author'];
                $data['image'] = !empty($trackData[0]->artwork_url) ? $trackData[0]->artwork_url : null;

                $data['link'] = $trackData[0]->permalink_url;
                $data['track_name'] = $titleData['title'];

                $data['source_id'] = $trackData[0]->id;

                $data['markets'] = null;

                return $data;
            }
            return false;

        } catch (\Exception $e) {
            return false;
        }
    }

    private function searchItunesTrack($title,$artist){
        $itunes = new Itunes();
        $trackData = $itunes->search($title,$artist);

        if($trackData->results[0]){

            $data = [];
            $data['type'] = 'itunes';

            $data['artist'] = $trackData->results[0]->artistName;
            $data['image'] = !empty($trackData->results[0]->artworkUrl100) ? $trackData->results[0]->artworkUrl100 : null;

            $data['link'] = $trackData->results[0]->trackViewUrl;
            $data['track_name'] = $trackData->results[0]->trackName;

            $data['source_id'] = $trackData->results[0]->trackId;
            $data['preview_url'] = $trackData->results[0]->previewUrl;

            $data['markets'] = null;

            return $data;
        }

        return $trackData;
    }

    private function searchAmazonTrack($title,$artist){

        $trackData = AmazonProduct::search('All',$title.'+'.$artist);

        if($trackData['Items']['Item'][0]){

            $data = [];
            $data['type'] = 'amazon_music';

            $data['artist'] = $trackData['Items']['Item'][0]['ItemAttributes']['Creator'];
            $data['image'] = !empty($trackData['Items']['Item'][0]['LargeImage']['URL']) ? $trackData['Items']['Item'][0]['LargeImage']['URL'] : null;

            $data['link'] = $trackData['Items']['Item'][0]['DetailPageURL'];
            $data['track_name'] = $trackData['Items']['Item'][0]['ItemAttributes']['Title'];

            $data['source_id'] = $trackData['Items']['Item'][0]['ASIN'];

            $data['markets'] = null;

            return $data;
        }

        return $trackData;
    }

    private function checkSpotifyTrack($link){
        //Getting track id from link
        $parts = explode("/", $link);
        $track_id = end($parts);

        $spotify = new Spotify();
        $trackData = $spotify->getTrack($track_id);


        $data = [];
        $data['artist'] = $trackData->artists[0]->name;
        $data['track_name'] = $trackData->name;
        $data['preview_url'] = $trackData->preview_url;
        $data['image'] = $trackData->album->images ? $trackData->album->images[0] : null;
        $data['source_id'] = $trackData->id;


        $data['slug'] = $this->generateSlug($trackData->name);

        if($trackData){
            return $data;
        }

        return false;
    }

    private function generateSlug($str){

        $slug = str_slug($str);
        $link = Link::select('slug')->where('slug',$slug)->get();

        while(count($link) > 0){

            $slug = $slug.'1';
            $link = Link::select('slug')->where('slug',$slug)->get();

        }

        return $slug;
    }

    private function getDeezerTrack($link){
        //Getting track id from link
        $parts = explode("/", $link);
        $track_id = end($parts);

        $deezer = new Deezer();
        $trackData = $deezer->getTrack($track_id);

        return $trackData;
    }

    private function getBeatPortTrack($link){
        //Getting track id from link
        $parts = explode("/", $link);
        $track_id = end($parts);

        $beatport = new Beatport();
        $trackData = $beatport->getTrack($track_id);

        return $trackData;
    }

    private function getYouTubeTrack($link){
        //Getting track id from link
        $pos = strpos($link,'v=');
        $track_id = substr($link,$pos+2);


        $youtube = new Youtube();
        $trackData = $youtube->getTrack($track_id);

        return $trackData;
    }

    private function getSoundCloudTrack($link){

        $soundcloud = new SoundCloud();
        $trackData = $soundcloud->resolve($link);

        return $trackData;
    }

    private function getItunesTrack($link){
        $parts = explode("/", $link);
        $transcr = $parts[count($parts) - 2];

        $itunes = new Itunes();
        $trackData = $itunes->searchUnique($transcr);

        return $trackData;
    }

    private function checkAmazonMusicTrack($link){
        $parts = explode("/", $link);
        $track_id = end($parts);

        if(strpos($track_id,'?')){
            $track_id = substr($track_id,0,strpos($track_id,'?') );
        }

        $trackData = AmazonProduct::item($track_id);

        return $trackData;
    }

    private function getYouTubeTrackTitleAuthor($title){

        $data = [];
        $data['title'] = $title;
        $data['author'] = $title;


        //Getting track author and title
        $pos = strpos($title,'-');
        if($pos != false){
            $data['title'] = substr($title,$pos + 2);
            $data['author'] = substr($title,0,$pos - 1);
        }

        return $data;
    }
}