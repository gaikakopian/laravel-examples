<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Auth\Models\ApiKey;
use Api\V1\General\Exceptions\CannotCreateUserException;
use Api\V1\General\Models\UUser;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Api\Common\Exceptions\Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UsersService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    public function editUser($request){
        try{

            $data = $request->all();
            $user = User::find($request->apiKey->apikeyable_id);

            //Checking if image is uploaded.
            if(!is_null($request->photo)){
                $data['avatar'] = Storage::put('/images', $request->photo);
                Storage::delete($user->avatar);
            }

            // Checking whether user wants to edit his info.
            if($request->delete_avatar && !is_null($user->avatar)){
                Storage::delete($user->avatar);
                $data['avatar'] = NULL;
            }

            $user->update($data);

            return $user;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }

    public function changePassword($request){
        try{
            $user = User::find($request->apiKey->apikeyable_id);
            $user->password = Hash::make($request->new_password);
            $user->save();

            return $user;
        } catch (\Exception $e) {
            throw new CannotCreateUserException($e->getMessage());
        }
    }
}