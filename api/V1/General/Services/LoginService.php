<?php
namespace Api\V1\General\Services;

/**
 * File UserService.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Services
 * @su1bpackage RegisterService.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\V1\General\Exceptions\NoUserFoundWithUsernameException;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\User;
use Api\Common\Auth\Models\ApiKey;
use Api\V1\General\Models\UCodeMessage;
use Api\Common\Exceptions\NotFoundException;
use Api\Common\Exceptions\Exception;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * Class UserService
 *
 * Perform business operations for User
 *
 * @package    Api\V1\General\Services;
 * @subpackage UserService
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class LoginService extends Service
{
    use AuthenticatesUsers,ValidatesRequests;

    public function login($request){
        try{

            if (!$this->attemptLogin($request)){
                throw new NoUserFoundWithUsernameException('Wrong email or password');
            }
            $user = User::select('id','email')->where('email',$request->email)->first();
            $apikey = ApiKey::make($user,'user login with email');

            $data = [];
            $data['token'] = $apikey->key;
            $data['user'] = $user;

            return $data;

        } catch (\Exception $e) {
            throw new NoUserFoundWithUsernameException($e->getMessage());
        }
    }

    public function logout($request){
        try{
            $apikey = ApiKey::find($request->apiKey->id);
            if(!$apikey) throw new NotFoundException();
            $apikey->delete();
            return true;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}