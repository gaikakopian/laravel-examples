<?php
namespace Api\V1\General\Integrations;

use GuzzleHttp\Client;

class Itunes{

    private $url = 'https://itunes.apple.com/';

    public function __construct()
    {
    }

    /**
     * @param $url
     * @param array $body
     * @return mixed
     */
    private function requestGet($url)
    {
        $client = new Client();

        try {
            $response = $client->get($this->url . $url );
        } catch (Exception $e){
            return $e->getMessage();
        }

        $responseContent = $response->getBody()->getContents();

        return is_string($responseContent) ? json_decode($responseContent) : null;
    }

    /**
     * @return mixed|string
     */
    public function searchUnique($track_unique){

        try {
            return $this->requestGet('search?term='. $track_unique );
        } catch (Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * @return mixed|string
     */
    public function search($title,$artist){

        try {
            $result =  $this->requestGet('search?term='. $artist .'+'. $title );
            return $result;
        } catch (Exception $e){
            return $e->getMessage();
        }
    }

}