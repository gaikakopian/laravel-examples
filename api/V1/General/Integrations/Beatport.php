<?php
namespace Api\V1\General\Integrations;

use GuzzleHttp\Client;
use MoussaClarke\BeatportApi;

class Beatport{

    private $consumer = '7cf7754909d3893beaefea661a10858ef19e4e35';
    private $secret = '6e0a61261d880b81031db1207f369e23a7865e60';


    public function __construct()
    {
    }

    /**
     * @param $url
     * @param array $body
     * @return mixed
     */
    public function getTrack($id)
    {

        try {
            // auth parameters
            $parameters = [
                'consumer'=> $this->consumer, // Your Beatport API Key
                'secret' => $this->secret, // Your Beatport Secret Key
                'login' => 'gaikakopian', // Your Beatport Login Name
                'password' => 'Mesropmashtoc44220' // Your Beatport Password
            ];

            // query parameters
            $query = [
                'id' => $id, // The filter type
                'method' => 'tracks', // The Beatport API Method
                'perPage' => '150' // Number of results per page
            ];

            $api = new BeatportApi ($parameters); // initialise

            $response = $api->queryApi ($query); // run the query

            return $response;

        } catch (Exception $e){
            return $e->getMessage();
        }
    }

    public function search($artist,$title)
    {

        try {
            // auth parameters
            $parameters = [
                'consumer'=> $this->consumer, // Your Beatport API Key
                'secret' => $this->secret, // Your Beatport Secret Key
                'login' => 'gaikakopian', // Your Beatport Login Name
                'password' => 'Mesropmashtoc44220' // Your Beatport Password
            ];

            // query parameters
            $query = [
                'query' => $artist.' '.$title, // The filter type
                'method' => 'search', // The Beatport API Method
                'perPage' => '150', // Number of results per page
                'page' => 0
            ];

            $api = new BeatportApi ($parameters); // initialise

            $response = $api->queryApi ($query); // run the query

            return $response;

        } catch (Exception $e){
            return $e->getMessage();
        }
    }

}