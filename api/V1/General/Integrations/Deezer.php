<?php
namespace Api\V1\General\Integrations;

use GuzzleHttp\Client;

class Deezer{

    private $url = 'https://api.deezer.com/';

    public function __construct()
    {
    }

    /**
     * @param $url
     * @param array $body
     * @return mixed
     */
    private function requestGet($url)
    {
        $client = new Client();

        try {
            $response = $client->get($this->url . $url );
        } catch (Exception $e){
            return $e->getMessage();
        }

        $responseContent = $response->getBody()->getContents();

        return is_string($responseContent) ? json_decode($responseContent) : null;
    }

    /**
     * @return mixed|string
     */
    public function getTrack($url){

        try {
            return $this->requestGet('track/'.$url);
        } catch (Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * @return mixed|string
     */
    public function search($title,$artist,$type){

        try {
            return $this->requestGet('search?q='.$artist .' '. $title);
        } catch (Exception $e){
            return $e->getMessage();
        }
    }

}