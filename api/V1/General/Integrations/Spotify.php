<?php
namespace Api\V1\General\Integrations;

use GuzzleHttp\Client;

class Spotify{

    private $url = 'https://api.spotify.com/v1/';
    private $account_url = 'https://accounts.spotify.com';
    private $client_id = '61ba33a1a3914995b4e5337d24bdf46d';
    private $client_secret = '9d7663e5c6d34cc2a9473edd85047e5b';

    public function __construct()
    {
    }

    /**
     * @param $url
     * @param array $body
     * @return mixed
     */
    private function request($url , $body = [])
    {
        $client = new Client([
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode($this->client_id.':'.$this->client_secret),
                'AMRO-Request-Info' => 'UEFSVE5FUkFQSTpOZXdBY2NvdW50X1BhcnRuZXJMZWFk'
            ]
        ]);

        try {
            $response = $client->post($this->account_url . $url , [
                'form_params' => $body
            ]);
        } catch (Exception $e){
            return $e->getMessage();
        }

        $responseContent = $response->getBody()->getContents();

        return is_string($responseContent) ? json_decode($responseContent) : null;
    }

    /**
     * @param $url
     * @param array $body
     * @return mixed
     */
    private function requestGet($url,$token)
    {
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ]
        ]);

        try {
            $response = $client->get($this->url . $url );
        } catch (Exception $e){
            return $e->getMessage();
        }

        $responseContent = $response->getBody()->getContents();

        return is_string($responseContent) ? json_decode($responseContent) : null;
    }

    /**
     * @return mixed|string
     */
    public function getTrack($url){

        try {
            $body = [
                'grant_type' => 'client_credentials'
            ];
            $res = $this->request('/api/token',$body);
            return $this->requestGet('tracks/'.$url,$res->access_token);
        } catch (Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * @return mixed|string
     */
    public function search($title,$artist,$type){

        try {
            $body = [
                'grant_type' => 'client_credentials'
            ];
            $res = $this->request('/api/token',$body);
            return $this->requestGet('search?q='.$artist .' '. $title .'&type='.$type,$res->access_token);
        } catch (Exception $e){
            return $e->getMessage();
        }
    }

}