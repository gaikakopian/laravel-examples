<?php
namespace Api\V1\General\Integrations;

use GuzzleHttp\Client;

class SoundCloud{

    private $url = 'http://api.soundcloud.com/';
    private $client_id = 'f7d5e64d4daedb45d91f1f04bf884ab2';
//    private $client_secret = 'e0ad508cf056521a2c437edd6090286b';

    public function __construct()
    {
    }

    /**
     * @param $url
     * @param array $body
     * @return mixed
     */
    private function requestGet($url)
    {
        $client = new Client();

        try {
            $response = $client->get($this->url . $url );
        } catch (Exception $e){
            return $e->getMessage();
        }

        $responseContent = $response->getBody()->getContents();

        return is_string($responseContent) ? json_decode($responseContent) : null;
    }

    /**
     * @return mixed|string
     */
    public function resolve($link){

        try {
            return $this->requestGet('resolve?url='. $link . '&client_id=' .$this->client_id);
        } catch (Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * @return mixed|string
     */
    public function search($title,$artist){

        try {
            $result =  $this->requestGet('tracks/?q='. $artist .' '. $title .'&client_id='.$this->client_id);
            return $result;
        } catch (Exception $e){
            return $e->getMessage();
        }
    }

}