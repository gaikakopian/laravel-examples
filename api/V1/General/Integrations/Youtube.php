<?php
namespace Api\V1\General\Integrations;

use GuzzleHttp\Client;

class Youtube{

    private $url = 'https://www.googleapis.com/youtube/v3/';
    private $key = 'AIzaSyA0E1bKNj27AtfA1_p77w4xhLvT55w85Tw';

    public function __construct()
    {
    }

    /**
     * @param $url
     * @param array $body
     * @return mixed
     */
    private function requestGet($url)
    {
        $client = new Client();

        try {
            $response = $client->get($this->url . $url );
        } catch (Exception $e){
            return $e->getMessage();
        }

        $responseContent = $response->getBody()->getContents();

        return is_string($responseContent) ? json_decode($responseContent) : null;
    }

    /**
     * @return mixed|string
     */
    public function getTrack($id){

        try {
            return $this->requestGet('videos/?part=snippet&key='. $this->key . '&id=' .$id);
        } catch (Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * @return mixed|string
     */
    public function search($title,$artist){

        try {
            $result =  $this->requestGet('search?part=snippet&type=track&key='. $this->key .'&q='.$artist .' '. $title);
            return $result;
        } catch (Exception $e){
            return $e->getMessage();
        }
    }

}