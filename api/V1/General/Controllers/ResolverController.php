<?php
namespace Api\V1\General\Controllers;

/**
 * File UserController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage UserController.php
 * @author     Davit Okhikyan <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Services\ResolverService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * Class UserController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage UserController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class ResolverController extends ApiController
{

    protected $resolverService;

    /**
     * LoginController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->resolverService = new ResolverService();
    }

    public function resolve(Request $request){

        try {
            $api_key = $this->resolverService->resolve($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }

    public function getType(Request $request){

        try {
            $api_key = $this->resolverService->getType($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }

    public function scan(Request $request){

        try {
            $api_key = $this->resolverService->scan($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }

    public function checkSlug(Request $request){

        try {
            $api_key = $this->resolverService->checkSlug($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }


}