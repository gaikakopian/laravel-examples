<?php
namespace Api\V1\General\Controllers;

/**
 * File UserController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage UserController.php
 * @author     Davit Okhikyan <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Services\LinksService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Api\V1\General\Services\LoginService;

/**
 * Class UserController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage UserController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class LinksController extends ApiController
{

    protected $linksService;

    /**
     * LoginController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->linksService = new LinksService();
    }

    public function create(Request $request){
        try {
            $api_key = $this->linksService->create($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function getLink(Request $request){
        try {
            $api_key = $this->linksService->getLink($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function getList(Request $request){
        try {
            $api_key = $this->linksService->getList($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function edit(Request $request){
        try {
            $api_key = $this->linksService->edit($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function delete(Request $request){
        try {
            $api_key = $this->linksService->delete($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function getLinkSlug(Request $request){
        try {
            $api_key = $this->linksService->getLinkSlug($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function search(Request $request){
        try {
            $api_key = $this->linksService->search($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }
}