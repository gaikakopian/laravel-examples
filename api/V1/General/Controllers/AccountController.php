<?php
namespace Api\V1\General\Controllers;

/**
 * File UserController.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Controllers
 * @subpackage UserController.php
 * @author     Davit Okhikyan <gaikakopian94@gmail.com>
 */

use Api\Common\ApiController;
use Api\Common\Errors\ErrorPrefix;
use Api\Common\Helpers\HttpCode;
use Api\Common\Exceptions\Exception;
use Api\V1\General\Services\AccountService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Api\V1\General\Services\LoginService;

/**
 * Class UserController
 *
 * @package    Api\V1\General\Controllers;
 * @subpackage UserController
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class AccountController extends ApiController
{

    protected $accountService;

    /**
     * LoginController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->accountService = new AccountService();
    }
    /**
     * Login user
     * POST /auth
     *
     * @param string $api_key The user api key
     * @param string $login The user login
     * @param string $password The user password
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request){

        try {
            $api_key = $this->accountService->create($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }

    /**
     * Login user
     * POST /auth
     *
     * @param string $api_key The user api key
     * @param string $login The user login
     * @param string $password The user password
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request){

        try {
            $api_key = $this->accountService->edit($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }

    /**
     * Login user
     * POST /auth
     *
     * @param string $api_key The user api key
     * @param string $login The user login
     * @param string $password The user password
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request){

        try {
            $api_key = $this->accountService->delete($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }

    public function givePermission(Request $request){

        try {
            $api_key = $this->accountService->givePermission($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);

    }

    public function deletePermission(Request $request){
        try {
            $api_key = $this->accountService->deletePermission($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function getList(Request $request){
        try {
            $api_key = $this->accountService->getList($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }

    public function getAccount(Request $request){
        try {
            $api_key = $this->accountService->getAccount($request);
            $result     = $this->response->setSuccess($api_key);
            $httpStatus = HttpCode::OK;
        } catch (Exception $e) {
            $httpStatus = HttpCode::INTERNAL_SERVER_ERROR;
            $result     = $this->response->setErrorFromException(ErrorPrefix::GENERAL, $e);
        }

        return Response::json($result, $httpStatus);
    }


}