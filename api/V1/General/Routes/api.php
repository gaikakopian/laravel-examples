<?php

use Api\V1\General\Middlewares\CheckRegisterEmail;
use Api\V1\General\Middlewares\CheckLoginEmail;
use Api\V1\General\Middlewares\CheckEditUser;
use Api\V1\General\Middlewares\CheckChangePassword;
use Api\V1\General\Middlewares\CheckCreateAccount;
use Api\V1\General\Middlewares\CheckEditAccount;
use Api\V1\General\Middlewares\CheckDeleteAccount;
use Api\V1\General\Middlewares\CheckGivePermissionAccount;
use Api\V1\General\Middlewares\CheckDeletePermissionAccount;
use Api\V1\General\Middlewares\CheckGetAccount;
use Api\V1\General\Middlewares\CheckCreateLink;
use Api\V1\General\Middlewares\CheckGetLink;
use Api\V1\General\Middlewares\CheckEditLink;
use Api\V1\General\Middlewares\CheckDeleteLink;
use Api\V1\General\Middlewares\CheckResolvingUrl;
use Api\V1\General\Middlewares\CheckCreateClick;
use Api\V1\General\Middlewares\CheckPreviewClick;
use Api\V1\General\Middlewares\CheckServiceClick;
use Api\V1\General\Middlewares\CheckGetClicksLink;
use Api\V1\General\Middlewares\CheckGetLinkType;
use Api\V1\General\Middlewares\CheckScanningUrl;
use Api\V1\General\Middlewares\CheckSearchLink;



Route::prefix('general')->group(function () {

    // Register email
    Route::post('/register' , 'RegisterController@register')->middleware([CheckRegisterEmail::class]);

    // Login email
    Route::post('/login' , 'LoginController@login')->middleware([CheckLoginEmail::class]);

    // Forgot password
    Route::post('/forgot-password' , 'ForgotPasswordController@sendRequest');

    //Get link user
    Route::get('/links/item-slug/{slug}' , 'LinksController@getLinkSlug');

    //Clicks
    Route::prefix('clicks')->group(function () {
        Route::post('/create' , 'ClicksController@create')->middleware([CheckCreateClick::class]);
        Route::get('/preview/{id}' , 'ClicksController@preview')->middleware([CheckPreviewClick::class]);
        Route::get('/service/{click_id}/{service_id}' , 'ClicksController@service')->middleware([CheckServiceClick::class]);
    });

    //logout
    Route::middleware(['auth.apikey'])->group(function () {
        // Forgot password
        Route::get('/logout' , 'LoginController@logout');

        //Countries
        Route::prefix('countries')->group(function () {
            Route::get('/list' , 'CountriesController@getList');
        });

        //Cities
        Route::prefix('cities')->group(function () {
            Route::get('/list' , 'CitiesController@getList');
        });

        //User
        Route::prefix('users')->group(function () {
            Route::post('/edit' , 'UsersController@editUser')->middleware([CheckEditUser::class]);
            Route::post('/change-password' , 'UsersController@changePassword')->middleware([CheckChangePassword::class]);
        });

        //Account
        Route::prefix('accounts')->group(function () {
            Route::post('/create' , 'AccountController@create')->middleware([CheckCreateAccount::class]);
            Route::post('/edit/{id}' , 'AccountController@edit')->middleware([CheckEditAccount::class]);
            Route::delete('/delete/{id}' , 'AccountController@delete')->middleware([CheckDeleteAccount::class]);
            Route::post('/give-permission' , 'AccountController@givePermission')->middleware([CheckGivePermissionAccount::class]);
            Route::delete('/delete-permission/{id}' , 'AccountController@deletePermission')->middleware([CheckDeletePermissionAccount::class]);
            Route::get('/list' , 'AccountController@getList');
            Route::get('/item/{id}' , 'AccountController@getAccount')->middleware([CheckGetAccount::class]);
        });

        //Shop
        Route::prefix('shops')->group(function () {
            Route::get('/list' , 'ShopsController@getList');
        });

        //List
        Route::prefix('links')->group(function () {
            Route::post('/create' , 'LinksController@create')->middleware([CheckCreateLink::class]);
            Route::get('/item/{id}' , 'LinksController@getLink')->middleware([CheckGetLink::class]);
            Route::post('/edit/{id}' , 'LinksController@edit')->middleware([CheckEditLink::class]);
            Route::delete('/delete/{id}' , 'LinksController@delete')->middleware([CheckDeleteLink::class]);
            Route::get('/list/{account_id}' , 'LinksController@getList');
            Route::get('/search/{account_id}' , 'LinksController@search')->middleware([CheckSearchLink::class]);
        });

        //Link resolver
        Route::prefix('link-resolver')->group(function () {
            Route::get('/resolve' , 'ResolverController@resolve')->middleware([CheckResolvingUrl::class]);
            Route::post('/scan-track' , 'ResolverController@scan')->middleware([CheckScanningUrl::class]);
            Route::get('/get-type' , 'ResolverController@getType')->middleware([CheckGetLinkType::class]);
            Route::get('/check-slug/{slug}' , 'ResolverController@checkSlug');
        });

        //Clicks
        Route::prefix('clicks')->group(function () {
            Route::get('/count/{link_id}' , 'ClicksController@count')->middleware([CheckGetClicksLink::class]);
        });
    });
});
