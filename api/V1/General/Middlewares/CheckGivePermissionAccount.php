<?php
namespace Api\V1\General\Middlewares;

/**
 * File CheckMasterServiceExist
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Middlewares
 * @subpackage CheckMasterExists.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;
use Api\Common\Response;
use Api\V1\General\Exceptions\CannotGivePermissionException;
use App\Models\Account;
use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as HttpResponse;
use Illuminate\Support\Facades\Validator;
use Api\V1\General\Exceptions\InvalidDataException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
/**
 * Class CheckMasterServiceExist
 *
 * @package   Api\V1\SmartMailer\Middlewares;
 * @subpackage CheckMasterServiceExist.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CheckGivePermissionAccount
{
    use AuthenticatesUsers,ValidatesRequests;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $res = new Response(null, null, null);

        try {

            $rules = [
                'account_id' => ['required', 'string', 'min:3'],
                'user_id' => ['required', 'string', 'min:3'],
                'role' => 'required|in:user,manager,admin',
            ];

            $validator = Validator::make($request->all(), $rules, []);

            if ($validator->fails()) {
                throw new CannotGivePermissionException($validator->errors());
            }

            $user = User::find($request->user_id);
            if(!$user){
                throw new CannotGivePermissionException('User not found');
            }

            $account = Account::where('admin_id',$request->apiKey->apikeyable_id)->where('_id',$request->account_id)->first();
            if(!$account){
                throw new CannotGivePermissionException('Account not found');
            }

        } catch (Exception $e) {
            $result = $res->setErrorFromException(1110, $e);

            return HttpResponse::json($result, 500);
        }

        return $next($request);
    }

}