<?php
namespace Api\V1\General\Middlewares;

/**
 * File CheckMasterServiceExist
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Middlewares
 * @subpackage CheckMasterExists.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;
use Api\Common\Response;
use Api\V1\General\Exceptions\CannotChangePasswordException;
use Api\V1\General\Models\UCarModel;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as HttpResponse;
use Illuminate\Support\Facades\Validator;
use Api\V1\General\Exceptions\InvalidDataException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\User;
/**
 * Class CheckMasterServiceExist
 *
 * @package   Api\V1\SmartMailer\Middlewares;
 * @subpackage CheckMasterServiceExist.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CheckChangePassword
{
    use AuthenticatesUsers,ValidatesRequests;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $res = new Response(null, null, null);

        try {

            $rules = [
                'password' => ['required', 'string', 'min:6'],
                'new_password' => ['required', 'string', 'min:6', 'confirmed'],
            ];

            $validator = Validator::make($request->all(), $rules, []);

            if ($validator->fails()) {
                throw new CannotChangePasswordException($validator->errors());
            }

            $user = User::find($request->apiKey->apikeyable_id);
            $request->merge(['email' => $user->email]);

            if(!$this->attemptLogin($request)){
                throw new CannotChangePasswordException($validator->errors());
            }

        } catch (Exception $e) {
            $result = $res->setErrorFromException(1110, $e);

            return HttpResponse::json($result, 500);
        }

        return $next($request);
    }

}