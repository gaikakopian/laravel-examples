<?php
namespace Api\V1\General\Middlewares;

/**
 * File CheckMasterServiceExist
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Middlewares
 * @subpackage CheckMasterExists.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;
use Api\Common\Response;
use Api\V1\General\Exceptions\CannotUpdateLinkException;
use Api\V1\General\Models\UCarModel;
use App\Models\Account;
use App\Models\Link;
use App\Models\Shop;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as HttpResponse;
use Illuminate\Support\Facades\Validator;
use Api\V1\General\Exceptions\InvalidDataException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\User;
/**
 * Class CheckMasterServiceExist
 *
 * @package   Api\V1\SmartMailer\Middlewares;
 * @subpackage CheckMasterServiceExist.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class CheckEditLink
{
    use AuthenticatesUsers,ValidatesRequests;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $res = new Response(null, null, null);

        try {

            $rules = [
                'account_id' => ['required', 'string'],
                'link_shops' => ['required'],
                'title' => ['required','string'],
                'preview_source_id' => ['required', 'string'],
            ];

            $validator = Validator::make($request->all(), $rules, []);

            if ($validator->fails()) {
                throw new CannotUpdateLinkException($validator->errors());
            }

            $link = Link::find($request->id);

            if(!$link){
                throw new CannotUpdateLinkException('The link is not found.');
            }

            $account = Account::where('_id',$request->account_id)->where('admin_id',$request->apiKey->apikeyable_id)->first();
            if(!$account){
                throw new CannotUpdateLinkException('You are not permitted to do this action');
            }

            $preview_shop = Shop::find($request->preview_source_id);
            if(!$preview_shop){
                throw new CannotUpdateLinkException('The preview source id you have mentioned is not found');
            }

        } catch (Exception $e) {
            $result = $res->setErrorFromException(1110, $e);

            return HttpResponse::json($result, 500);
        }

        return $next($request);
    }

}