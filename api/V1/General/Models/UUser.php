<?php namespace Api\V1\General\Models;


/**
 * File SalonAgendas.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @subpackage SalonAgendas.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class SalonAgendas
 *
 * @package    Api\Common\Auth\Models;
 * @subpackage ApiKey
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UUser extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'users';

    protected $fillable = [
        'email', 'name','password'
    ];

}