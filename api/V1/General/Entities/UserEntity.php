<?php
namespace Api\V1\General\Entities;

/**
 * File UserEntity.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Entities
 * @subpackage UserEntity.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 * @copyright  2018 Kyvio.com All rights reserved.
 */

/**
 * Class UserEntity
 *
 * @package    Api\V1\General\Entities
 * @subpackage UserEntity
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class UserEntity extends Entity implements \JsonSerializable
{

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var int
     */
    private $accountId;

    /**
     * @var int
     */
    private $isAdmin;

    /**
     * @var int
     */
    private $picture;

    /**
     * @var int
     */
    private $created_at;

    /**
     * UserEntity constructor.
     *
     * @param array $data
     *
     */
    public function __construct(array $data)
    {
        $this->userId    = $this->checkArrayIndex($data , 'userId');
        $this->email     = $this->checkArrayIndex($data , 'email');
        $this->username  = $this->checkArrayIndex($data , 'username');
        $this->firstName = $this->checkArrayIndex($data ,'firstName');
        $this->lastName  = $this->checkArrayIndex($data , 'lastName');
        $this->accountId = $this->checkArrayIndex($data , 'accountId');
        $this->isAdmin   = $this->checkArrayIndex($data , 'isAdmin');
        $this->picture   = $this->checkArrayIndex($data , 'picture');
        $this->created_at = $this->checkArrayIndex($data , 'createdAt');
    }

    /**
     *
     * @author     Gaik Akopian <gaikakopian94@gmail.com>
     *
     * @return array
     */
    public function jsonSerialize()
    {
        $return = $this;

        return get_object_vars($return);
    }


}