<?php
namespace Api\V1\General\Providers;

/**
 * File RouteServiceProvider.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\General\Providers
 * @subpackage RouteServiceProvider.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */


use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;


/**
 * Class RouteServiceProvider
 *
 * @package    Api\V1\General\Providers;
 * @subpackage RouteServiceProvider
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Api\V1\General\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        Route::prefix('api')
            ->namespace($this->namespace)
            ->group(base_path('api/V1/General/Routes/api.php'));
    }
}