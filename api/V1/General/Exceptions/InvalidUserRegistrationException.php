<?php namespace Api\V1\General\Exceptions;

/**
 * File InvalidUserRegistrationException.php
 *
 * PHP version 7
 *
 * @category   PHP
 * @package    Api\V1\SmartMailer\Exceptions
 * @subpackage InvalidUserRegistrationException.php
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */

use Api\Common\Exceptions\Exception;

/**
 * Class InvalidUserRegistrationException
 *
 * Generated when User Registration is invalid
 *
 * @package    Api\V1\SmartMailer\Exceptions;
 * @subpackage InvalidUserRegistrationException
 * @author     Gaik Akopian <gaikakopian94@gmail.com>
 */
class InvalidUserRegistrationException extends Exception
{

}